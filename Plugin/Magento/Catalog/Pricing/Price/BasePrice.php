<?php

namespace BernardRobbins\ZeroPriceTierFix\Plugin\Magento\Catalog\Pricing\Price;

class BasePrice
{

    protected $value = false;

    public function afterGetValue(
        \Magento\Catalog\Pricing\Price\BasePrice $subject,
        $result
    ) {
        if ($this->value === null) {
            $this->value = false;
            foreach ($subject->priceInfo->getPrices() as $price) {
                if ($price instanceof BasePriceProviderInterface && $price->getValue() !== false) {
                    $this->value = min($price->getValue(), is_numeric($this->value) ? $this->value : $price->getValue());
                }
            }
        }

        return $this->value;
    }
}
