Intended to fix:
https://github.com/magento/magento2/issues/18268

2 ways to install:
Copy source to app/code/BernardRobbins/ZeroPriceTierFix
Run bin/magento setup:upgrade

Add these lines to composer.json
"require": {
"tmp/module-zerotierpricefix": "1.0.*"

"repositories": [
{
    "type": "vcs",
    "url": "git@bitbucket.org:BernardRobbins/zeropricetierfix.git"
}
Run bin/magento setup:upgrade